export default [{
		imgUrl: 'https://cdn.zhoukaiwen.com/project1.jpeg',
		type: '微信公众号/小程序',
		name: '前后台商城，包含分销、秒杀、拼图、支付、砍价',
		time: '2020-12-05',
		user: [{
			like: '10',
			read: '201',
			use: '15'
		}],
		content: ''
	},
	{
		imgUrl: 'https://cdn.zhoukaiwen.com/dataVIS3.png',
		type: '数据可视化大屏',
		name: '数据可视化大屏电子沙盘集合，包含多种模版',
		time: '2021-05-15',
		user: [{
			like: '10',
			read: '201',
			use: '15'
		}],
		content: ''
	},
	{
		imgUrl: 'https://cdn.zhoukaiwen.com/ybss_jt.png',
		type: '后台管理系统',
		name: 'ToG端，一标三实信息管理平台，部分城市已上线',
		time: '2020-12-05',
		user: [{
			like: '10',
			read: '201',
			use: '15'
		}],
		content: ''
	}
]
